const { initConfig } = require('./utils/config');

// Initialize the config files
initConfig(['mongoDB', 'node', 'paths', 'postgreSQL']);

const { initWeb, registerCRUD, startListening } = require('./rest/core');

// Initialize Express
initWeb();

// Register listeners for the API
registerCRUD(require('./rest/contact'));

// Start listening on the configured port
startListening();

// Database functions for contacts

const { getConfig } = require('../utils/config.js');
const { getConnection } = require('./core');

const mongoose = require('mongoose');

const addressSchema = new mongoose.Schema({
    street: String,
    city: String,
    state: String,
    postalCode: String,
    country: String,    
});

const contactSchema = new mongoose.Schema({
        firstName: String,
        lastName: String,
        birthdate: String,
        addresses: [addressSchema],
        phoneNumbers: [String],
        emails: [String],
        position: Object
    }
);

// Returns a contact's data, with pre-calculated fields for searching
contactSchema.methods.withHelperValues = function() {
    let data = this.toObject();
    let defaultAddress = data.addresses && data.addresses[0] ? data.addresses[0] : null;
    return {
        ...data,
        address: defaultAddress ? `${defaultAddress.street}, ${defaultAddress.city}, ${defaultAddress.state} ${defaultAddress.postalCode} ${defaultAddress.country}` : null,
        phoneNumber: data.phoneNumbers && data.phoneNumbers[0],
        email: data.emails && data.emails[0]
    };
};

let ContactModel = null;

// Returns a promise that resolves a model for a contact, creating to it if necessary, and connecting the database if necessary
const getModel = function() {
    return new Promise(function(resolve, reject) {
        getConnection().then(function(connection) {
            if (!ContactModel) {
                let collectionName = getConfig('mongoDB.models.contact.collection')
                ContactModel = connection.model('Contact', contactSchema, collectionName);
            }
            resolve(ContactModel);
        });
    });
};

// Returns a promise that deletes a contact given the id, and resolves to the deleted contact
const deleteContact=function(id) {
    return new Promise(function(resolve, reject) {
        if (id) {
            getModel().then(function(Contacts) {
                Contacts.findByIdAndDelete(id, function(error, deletedContact) {
                    if (error) {
                        reject(error);
                    } else {
                        resolve(deletedContact);
                    }
                });
            });
        } else {
            reject();
        }
    });
}

// Returns a promise that resolves a single contact matching ID
const getContactByID = function(id) {
    return new Promise(function(resolve, reject) {
        getModel().then(function(Contacts) {
            Contacts.findById(id, function(error, contact) {
                if (error) {
                    reject(error)
                } else {
                    resolve(contact.withHelperValues());
                }
            });
        });
    });
}

// Returns a promise that resolves an array of all contacts
const getContacts = function() {
    return new Promise(function(resolve, reject) {
        getModel().then(function(Contacts) {
            Contacts.find({}, function(error, contacts) {
                if (error) {
                    reject(error);
                } else {
                    let newContacts = contacts.map(contact => contact.withHelperValues());
                    resolve(newContacts);
                }
            });        
        });
    });
};

// Returns a promise that resolves to a new contact with blank ID
const getNewContact = function() {
    return new Promise(function(resolve, reject) {
        getModel().then(function(Contacts) {
            let newContact = new Contacts({
                firstName: '',
                lastName: ''
            });
            newContact.id = null;
            newContact._id = null;
            resolve(newContact);
        });
    });

};

// Returns a promise that upserts the given contact and resolves to the new contact (id populated if new)
const upsertContact=function(contact) {
    return new Promise(function(resolve, reject) {
        getModel().then(function(Contacts) {            
            if (contact._id) {
                Contacts.findByIdAndUpdate(contact._id, {$set: contact}, {new:true}, function(error, newContact) {
                    if (error) {
                        reject(error);
                    } else {
                        resolve(newContact.withHelperValues());
                    }
                });
            } else {
                contact._id = mongoose.Types.ObjectId();
                Contacts.create(contact, function(error, newContact) {
                    if (error) {
                        reject(error);
                    } else {
                        resolve(newContact.withHelperValues());                        
                    }
                });
            }
        });
    });
};

module.exports = {
    deleteContact,
    getContacts,
    getContactByID,
    getNewContact,
    upsertContact
};
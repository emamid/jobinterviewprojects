// Database functions not specific to a collection

const { getConfig } = require('../utils/config.js');
const mongoose = require('mongoose');

let connection = null;

// Return a promise to the database connection, opening it if needed
const getConnection = function() {
    let server = getConfig('mongoDB.server');
    let port = getConfig('mongoDB.port');
    let database = getConfig('mongoDB.database');
    if (connection) {
        return Promise.resolve(connection);
    } else {
        return new Promise(function(resolve,reject) {
            const url = 'mongodb://'+server+':'+port;
            connection=mongoose.createConnection(url, {
                dbName: database,
                useNewUrlParser: true
            }, function(error) {        
                console.log(error ? JSON.stringify(error) : database+ ' connected');
                if (error) {
                    reject(error);                
                } else {
                    resolve(connection);                
                }
            });			
        });
    }
};

module.exports = {
    getConnection
};
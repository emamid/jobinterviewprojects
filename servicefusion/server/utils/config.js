const _ = require('lodash');
const assert = require('assert');
const pathUtils = require('path');

// Assumes the following folder structure:
//
// /client
// /server
// /server/config

const serverPath = require('app-root-path');
const workspacePath = _.replace(_.replace(serverPath, '/server', ''), '\\server', '');

console.log('serverPath='+serverPath);
console.log('workspacePath='+workspacePath);

let configData = null;

// Returns the contents of /server/config/subPath.json
const requireConfig = function(subPath) {
	return require(serverPath+'/config/'+subPath+'.json');	
};

// Get the config value based on the dot path, for example db.port
const getConfig = function(configPath) {
	assert(configData, 'Called getConfig before initConfig');
	return _.get(configData,configPath);
};

// As getConfig, but allows {server} and {workspace} as variables, and changes \ / chars
// to conform to the host OS
const getFilePath = function(configPath) {
	let result = getConfig(configPath);
	result = _.replace(result, '{server}', serverPath);
	result = _.replace(result, '{workspace}', workspacePath);
	return pathUtils.normalize(result);
};

// Either a config object or an array of keys. Use keys unless there is a neeed to
// map the topmost value in the dot path "x" to something other than "/server/config/x.json"
const initConfig = function(newConfigData) {
	assert(configData === null, 'Calling initConfig a second time');
	if (_.isArray(newConfigData)) {
		configData = {};
		for (let key of newConfigData) {
			configData[key] = requireConfig(key);
		}	
	} else {
		configData = newConfigData;
	}
};

module.exports = {
	getConfig,
	getFilePath,
	initConfig,
	requireConfig
};
-- Drop table

-- DROP TABLE public.addresses

CREATE TABLE public.addresses (
	contact_id bpchar(24) NULL,
	street varchar NULL,
	city varchar NULL,
	state varchar NULL,
	postalcode varchar NULL,
	country varchar NULL,
	CONSTRAINT addresses_contacts_fk FOREIGN KEY (contact_id) REFERENCES contacts(_id) ON UPDATE CASCADE ON DELETE CASCADE
);
CREATE INDEX addresses_contact_id_idx ON public.addresses USING btree (contact_id);

-- Permissions

ALTER TABLE public.addresses OWNER TO postgres;
GRANT ALL ON TABLE public.addresses TO postgres;
GRANT INSERT, SELECT, UPDATE, DELETE, TRUNCATE ON TABLE public.addresses TO servicefusion;

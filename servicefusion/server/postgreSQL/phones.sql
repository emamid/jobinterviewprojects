-- Drop table

-- DROP TABLE public.phones

CREATE TABLE public.phones (
	contact_id bpchar(24) NOT NULL,
	phonenumber varchar NULL,
	CONSTRAINT phones_contacts_fk FOREIGN KEY (contact_id) REFERENCES contacts(_id) ON UPDATE CASCADE ON DELETE CASCADE
);
CREATE INDEX phones_contact_id_idx ON public.phones USING btree (contact_id);

-- Permissions

ALTER TABLE public.phones OWNER TO postgres;
GRANT ALL ON TABLE public.phones TO postgres;
GRANT INSERT, SELECT, UPDATE, DELETE, TRUNCATE ON TABLE public.phones TO servicefusion;

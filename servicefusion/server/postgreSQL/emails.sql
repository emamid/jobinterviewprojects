-- Drop table

-- DROP TABLE public.emails

CREATE TABLE public.emails (
	contact_id bpchar(24) NULL,
	email varchar NULL,
	CONSTRAINT emails_contacts_fk FOREIGN KEY (contact_id) REFERENCES contacts(_id) ON UPDATE CASCADE ON DELETE CASCADE
);
CREATE INDEX emails_contact_id_idx ON public.emails USING btree (contact_id);

-- Permissions

ALTER TABLE public.emails OWNER TO postgres;
GRANT ALL ON TABLE public.emails TO postgres;
GRANT INSERT, SELECT, UPDATE, DELETE, TRUNCATE ON TABLE public.emails TO servicefusion;

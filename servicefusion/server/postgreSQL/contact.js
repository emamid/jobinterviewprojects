// Database functions for contacts

const { getConfig } = require('../utils/config.js');
const { getConnectedClient } = require('./core');
const ObjectID = require('bson-objectid');

const tables = getConfig('postgreSQL.tables');

const contactFieldsSQL = 'firstname, lastname, birthdate, lat, lng, "_id"';

const firstPhoneSQL = `(select phonenumber FROM ${tables.phones} WHERE contact_id = _id limit 1)`;
const firstEmailSQL = `(select email FROM ${tables.emails} WHERE contact_id = _id limit 1)`;
const firstAddressSQL = `(select street FROM ${tables.addresses} WHERE contact_id = _id limit 1)`;
const listSQL = `select ${contactFieldsSQL}, ${firstAddressSQL}, ${firstEmailSQL}, ${firstPhoneSQL} from ${tables.contacts}`;

const getContactSQL = `select ${contactFieldsSQL} from ${tables.contacts} where _id=$1::text`;
const getPhonesSQL = `select phonenumber from ${tables.phones} where contact_id=$1::text`;
const getEmailsSQL = `select email from ${tables.emails} where contact_id=$1::text`;
const getAddressesSQL = `select street, city, state, postalcode, country from ${tables.addresses} where contact_id=$1::text`;

const deleteContactSQL = `delete from ${tables.contacts} where _id=$1::text`;

const insertPhoneSQL = `insert into ${tables.phones} (contact_id, phonenumber) values ($1::text, $2::text)`;
const insertEmailSQL = `insert into ${tables.emails} (contact_id, email) values ($1::text, $2::text)`;

const transformContactForList = function(contact) {
    return {
        _id: contact._id,
        firstName: contact.firstname,
        lastName: contact.lastname,
        birthdate: contact.birthdate,
        address: contact.street,
        email: contact.email,
        phoneNumber: contact.phonenumber,
        position: {
            lat: contact.lat,
            lng: contact.lng
        }
    }
}

const transformContactForGet = function(contact) {
    return {
        _id: contact._id,
        firstName: contact.firstname,
        lastName: contact.lastname,
        birthdate: contact.birthdate,
        position: {
            lat: contact.lat,
            lng: contact.lng
        }
    }
}

const transformPhoneForGet = function(phone) {
    return phone.phonenumber;
}

const transformEmailForGet = function(email) {
    return email.email;
}

const transformAddressForGet = function(address) {
    return {
        street: address.street,
        city: address.city,
        state: address.state,
        postalCode: address.postalcode,
        country: address.country
    }
}

// Returns a promise that deletes a contact given the id, and resolves to the deleted contact
const deleteContact=function(id) {
    return new Promise(function(resolve, reject) {
        if (id) {
            getConnectedClient()
                .then(function(client) {
                    client.query(deleteContactSQL, [id])
                        .then(function() {
                            resolve({
                                _id: id,
                                firstName: 'Deleted',
                                lastName: 'Contact'
                            });
                        })
                        .catch(function(error) {
                            console.log(error);
                            reject(error);
                        })
                })
                .catch(function(error) {
                    console.log(error);
                    reject(error);
                })
        } else {
            reject();
        }
    });
}

// Returns a promise that resolves a single contact matching ID
const getContactByID = function(id) {
    return new Promise(function(resolve, reject) {
        getConnectedClient()
            .then(function(client) {
                let contactQuery = client.query(getContactSQL, [id]);
                let phonesQuery = client.query(getPhonesSQL, [id]);
                let emailsQuery = client.query(getEmailsSQL, [id]);
                let addressesQuery = client.query(getAddressesSQL, [id]);
                let queries = Promise.all([contactQuery, phonesQuery, emailsQuery, addressesQuery]);
                queries
                    .then(function([contactResult, phonesResult, emailsResult, addressesResult]) {
                        let contact = contactResult.rows.map(transformContactForGet)[0];
                        if (contact) {
                            contact.phoneNumbers = phonesResult.rows.map(transformPhoneForGet);
                            contact.emails = emailsResult.rows.map(transformEmailForGet);
                            contact.addresses = addressesResult.rows.map(transformAddressForGet);
                        }
                        resolve(contact);
                    })
                    .catch(function(error) {
                        console.log(error);
                        reject(error);
                    });
            })
            .catch(function(error) {
                console.log(error);
                reject(error);
            })
    });
}

// Returns a promise that resolves an array of all contacts
const getContacts = function() {
    return new Promise(function(resolve, reject) {
        getConnectedClient()
            .then(function(client) {
                client.query(listSQL)
                    .then(function(result) {
                        resolve(result.rows.map(transformContactForList));
                    })
                    .catch(function(error) {
                        console.log(error);
                        reject(error);
                    })
            })
            .catch(function(error) {
                console.log(error);
                reject(error);
            })
    });
}

// Returns a promise that resolves to a new contact with blank ID
const getNewContact = function() {
    return Promise.resolve({
        firstName: '',
        lastName: '',
        addresses: [],
        emails: [],
        phoneNumbers: []
    });
}

// Creates a query that inserts populated fields, to avoid trying to set null values on the SQL table
const getInsertQuery = function(client, tableName, fieldValuesMap) {
    let fieldNames = [];
    let fieldValuePlaceholders = [];
    let fieldValueParams = [];
    let fieldCount = 1;
    for (let fieldName in fieldValuesMap) {
        let fieldValue = fieldValuesMap[fieldName];
        if (fieldValue) {
            fieldNames.push(fieldName);
            fieldValuePlaceholders.push('$'+fieldCount);
            fieldValueParams.push(fieldValue);
            fieldCount++;
        }
    }
    let insertSQL = `insert into ${tableName} (${fieldNames.join(', ')}) values (${fieldValuePlaceholders.join(', ')})`;
    return client.query(insertSQL, fieldValueParams);
}

// Returns a promise that upserts the given contact and resolves to the new contact (id populated if new)
const upsertContact = function(contact) {
    return new Promise(async function(resolve, reject) {
        try {
            let client = await getConnectedClient();
            if (contact._id) {
                await deleteContact(contact._id);
            } else {
                contact._id = ObjectID().toHexString();
            }
            let contactFieldValueMap = {
                firstname: contact.firstName,
                lastname: contact.lastName,
                birthdate: contact.birthdate,
                lat: contact.position ? contact.position.lat : null,
                lng: contact.position ? contact.position.lng : null,
                _id: contact._id
            }
            await getInsertQuery(client, tables.contacts, contactFieldValueMap);
            let detailQueries = [];
            if (contact.phoneNumbers) {
                for (let phoneNumber of contact.phoneNumbers) {
                    detailQueries.push(client.query(insertPhoneSQL, [contact._id, phoneNumber]));
                }
            }
            if (contact.emails) {
                for (let email of contact.emails) {
                    detailQueries.push(client.query(insertEmailSQL, [contact._id, email]));
                }
            }
            if (contact.addresses) {
                for (let address of contact.addresses) {
                    let addressFieldValueMap = {
                        street: address.street,
                        city: address.city,
                        state: address.state,
                        postalcode: address.postalCode,
                        country: address.country,
                        contact_id: contact._id
                    }
                    detailQueries.push(getInsertQuery(client, tables.addresses, addressFieldValueMap));
                }
            }
            await Promise.all(detailQueries);
            contact.phoneNumber = contact.phoneNumbers && contact.phoneNumbers[0];
            contact.email = contact.phoneNumbers && contact.emails[0];            
            contact.address = contact.addresses && contact.addresses[0] && contact.addresses[0].street;
            contact.phoneNumbers = null;
            contact.emails = null;
            contact.addresses = null;
            resolve(contact);    
        } catch(error) {
            reject(error);
        }
    });
}

module.exports = {
    deleteContact,
    getContacts,
    getContactByID,
    getNewContact,
    upsertContact
};
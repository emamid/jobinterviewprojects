-- Drop table

-- DROP TABLE public.contacts

CREATE TABLE public.contacts (
	firstname varchar NULL,
	lastname varchar NULL,
	birthdate varchar NULL,
	lat float8 NULL,
	lng float8 NULL,
	"_id" bpchar(24) NOT NULL,
	CONSTRAINT contacts_pk PRIMARY KEY (_id)
);
CREATE UNIQUE INDEX contacts__id_idx ON public.contacts USING btree (_id);

-- Permissions

ALTER TABLE public.contacts OWNER TO postgres;
GRANT ALL ON TABLE public.contacts TO postgres;
GRANT INSERT, SELECT, UPDATE, DELETE, TRUNCATE ON TABLE public.contacts TO servicefusion;

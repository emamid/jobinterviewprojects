const { getConfig } = require('../utils/config.js');
const { Client } = require('pg');

const getConnectedClient = function() {
	let client = new Client(getConfig('postgreSQL.connectionParams'));
	return new Promise(function(resolve, reject) {
		client.connect().then(function() {
			resolve(client);
		}).catch(function(error) {
			reject(error);
		});
	});
};

module.exports={
	getConnectedClient
}
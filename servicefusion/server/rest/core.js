const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors')
const assert = require('assert');
const _ = require('lodash');
const { getConfig, getFilePath } = require('../utils/config');


let app = null;
let listenerFactories = null;

// Initializes Express, sets up listenerFactories
const initWeb = function() {
    app = express();
    app.use(cors());
    app.use('/', express.static(getFilePath('paths.client')));
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({
        extended: false
    }));
    listenerFactories = {
        delete: app.delete.bind(app),
        get: app.get.bind(app),
        list: app.get.bind(app),
        post: app.post.bind(app)
    }
};

// Registers a single action (delete, get, list, post)
const registerAction = function(rootPath, actionType, actionConfig) {
    assert(actionConfig.handler, `Action ${actionType} does not have a handler`);
    let path = actionConfig.subpath ? rootPath + actionConfig.subpath : rootPath;
    let factory = listenerFactories[actionType];
    assert(factory, `Action ${actionType} does not have a corresponding listener factory`);
    factory(path, [actionConfig.handler]);
};

// Registers a set of CRUD actions with a common root path
const registerCRUD = function(config) {
    let rootPath = config.rootPath;
    assert(rootPath, 'CRUD group must have a root path');
    for (let actionType in config.actions) {
        registerAction(rootPath, actionType, config.actions[actionType]);
    }
};

// Await HTTP requests on the configured port
const startListening = function() {
    const nodePort = getConfig('node.port');    
    app.listen(nodePort, function() {
        console.log('Server listening on ', nodePort);
    });    
};

module.exports = {
    initWeb,
    registerAction,
    registerCRUD,
    startListening
};
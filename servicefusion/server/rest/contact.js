const { getConfig } = require('../utils/config');
const { getContactByID, getContacts, getNewContact, deleteContact, upsertContact } = require(`../${getConfig('node.db')}/contact`);

// Handles get requests at /api/contacts/:id, returns contact
const getHandler=async function(request, response, next) {
    let id = request.params.id;
    let dbPromise = id === 'new' ? getNewContact() : getContactByID(id);
    dbPromise
        .then(function(contact) {
            response.json(contact);
            if (next) {
                next();
            }
        })
        .catch(function(error) {
            console.log(error);
            response.status(404).json({error});
        });
};

// Handles delete requests at /api/contacts/:id, returns deleted contact
const deleteHandler=function(request, response, next) {
    let id = request.params.id;
    deleteContact(id)
        .then(function(deletedContact) {
            response.status(200).json(deletedContact);
        })
        .catch(function(error) {
            console.log(error);
            response.status(400).json({error});
        });
}

// Handles get requests at /api/contacts, returns { contacts: [] } containing all contacts
const listHandler=function(request, response, next) {
    getContacts()
        .then(function(contacts) {
            response.json({contacts});
            if (next) {
                next();
            }
        })
        .catch(function(error) {
            console.log(error);
            response.status(500).json({error});
        });
};

// Creates or updates a contact at /api/contacts, returns the updated/created contact
const postHandler=function(request, response, next) {
    let contact = request.body;
    upsertContact(contact)
        .then(function(updatedContact) {
            response.status(200).json(updatedContact);
            if (next) {
                next();
            }
        })
        .catch(function(error) {
            console.log(error);
            response.status(500).json({error});
        });
};

module.exports = {
    rootPath: '/api/contacts',
    actions: {
        delete: {
            handler: deleteHandler,
            subpath: '/:id'
        },
        get: {
            handler: getHandler,
            subpath: '/:id'
        },
        list: {
            handler: listHandler
        },
        post: {
            handler: postHandler
        }
    }
}
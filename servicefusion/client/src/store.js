import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const apiProtocol = null;
const apiHostName = null;
const apiPort = 3001;
const apiPathName = '/api';

let apiPath;

if (apiProtocol || apiPort || apiHostName) {
	let protocol = apiProtocol || location.protocol;
	let port = apiPort || location.port;
	let hostName = apiHostName || location.hostname;
	apiPath = `${protocol}//${hostName}:${port}${apiPathName}`;
	console.log('apiPath=', apiPath);
} else {
	apiPath = apiPathName;
}

const contactsPath = `${apiPath}/contacts`;
let geocoder = null;

export default new Vuex.Store({
	state: {
		contact: null,
		contactError: false,
		contactLoading: false,
		contacts: [],
		contactsError: false,
		contactsLoading: false,
		mode: null,
		multiRecordMode: 'table',
		searchValue: '',
		unfilteredContacts: []
	},
	mutations: {
		// Updates the contacts based on unfilteredContacts and searchValue
		applyFilter(state) {
			if (state.searchValue) {
				let internalSearchValue = state.searchValue.toLowerCase();
				state.contacts = state.unfilteredContacts.filter(contact => contact.searchableText.indexOf(internalSearchValue) !== -1);
			} else {
				state.contacts = state.unfilteredContacts;
			}
		},
		setAddresses(state, addresses) {
			state.contact.addresses = addresses;
		},
		setContact(state, contact) {
			state.contact = contact;
		},
		setContactError(state, contactError) {
			state.contact = contactError;
		},
		setContactLoading(state, contactLoading) {
			state.contactLoading = contactLoading;
		},
		setContactsError(state, contactsError) {
			state.contacts = contactsError;
		},
		setContactsLoading(state, contactsLoading) {
			state.contactsLoading = contactsLoading;
		},
		setEmails(state, emails) {
			state.contact.emails = emails;
		},
		setMode(state, mode) {
			state.mode = mode;
			if (mode !== 'record') {
				state.multiRecordMode = mode;
			}
		},
		setPhoneNumbers(state, phoneNumbers) {
			state.contact.phoneNumbers = phoneNumbers;
		},
		setSearchValue(state, searchValue) {
			state.searchValue = searchValue;
			this.commit('applyFilter');
		},
		setUnfilteredContacts(state, unfilteredContacts) {
			state.unfilteredContacts = unfilteredContacts.map(function(contact) {
				return {
					...contact, 
					searchableText: [contact.firstName, contact.lastName, contact.address, contact.phoneNumber, contact.email]
						.reduce((fullValue, fieldValue) => (fieldValue ? fullValue + ' ' + fieldValue : fullValue), '').toLowerCase()
				}
			}); 
			this.commit('applyFilter');
		}
	},
	actions: {
		// Deletes the contact defined by params.id and resolves to the deleted contact
		deleteContact(context, params) {
			return fetch(`${contactsPath}/${params.id}`, {
				method: 'DELETE'
			}).then(result => result.json())
			.then(function(deletedContact) {
				context.commit('setUnfilteredContacts', context.state.unfilteredContacts.filter(contact => deletedContact._id !== contact._id))
				return deletedContact;
			});
		},
		// Fetches the contact defined by params.id and resolves to it
		loadContact(context, params) {
			context.commit('setContactError', false);
			context.commit('setContactLoading', true);
			return fetch(`${contactsPath}/${params.id}`, {
				method: 'GET'
			}).then(result => result.json())
			.then(function(contact) {
				context.commit('setContact', contact);
				context.commit('setContactLoading', false);
				return contact;
			})
			.catch(function(error) {
				context.commit('setContactError', true);
				context.commit('setContactLoading', false);
				console.log(error)
			});
		},
		// Fetches all contacts and resolves to { contacts: []}
		loadContacts(context) {
			context.commit('setContactsError', false);
			context.commit('setContactsLoading', true);
			return fetch(contactsPath, {
				method: 'GET'
			}).then(result => result.json())
			.then(function(contacts) {				
				context.commit('setUnfilteredContacts', contacts.contacts);
				context.commit('setContactsLoading', false);
				return contacts;
			})
			.catch(function(error) {
				context.commit('setContactsError', true);
				context.commit('setContactsLoading', false);
				console.log(error)
			});
		},
		// Saves the contact defined by params.payload and resolves to the updated contact
		saveContact: async function(context, params) {
			let newParams = await context.dispatch('updateGeocoding', params);
			return fetch(contactsPath, {
				method: 'POST',
				headers: new Headers({
					'Content-Type': 'application/json; charset=utf-8'
				}),
				body: JSON.stringify(newParams.payload)
			}).then(result => result.json())
			.then(function(newContact) {
				if (params.payload._id) {
					context.commit('setUnfilteredContacts', [...context.state.unfilteredContacts.map(contact => contact._id === params.payload._id ? newContact : contact)]);
				} else {
					context.commit('setUnfilteredContacts', [...context.state.unfilteredContacts, newContact]);
				}
				return newContact;
			})
		},
		// Geocodes the contact if it has a default address, and resolves to the possibly-updated contact
		updateGeocoding(context, params) {			
			let defaultAddress = params && params.payload && params.payload.addresses && params.payload.addresses[0];
			if (defaultAddress) {
				if (!geocoder) {
					geocoder = new window.google.maps.Geocoder();
				}
				return new Promise(function(resolve, reject) {
					geocoder.geocode({'address': `${defaultAddress.street} ${defaultAddress.city} ${defaultAddress.state}`}, function(results, status) {
						if (status === 'OK') {
							let geocodedLocation = results[0].geometry.location;
							resolve({
								...params,
								payload: {
									...params.payload,
									position: {
										lat: geocodedLocation.lat(),
										lng: geocodedLocation.lng()
									}
								}
							});
						} else {
							console.log(`Geocode error ${status}`);
							// If there was a geocoding error, we still want to save to the server
							resolve(params);
						}
					});				
				});
			} else {
				return Promise.resolve(params);
			}
		}
	}
})

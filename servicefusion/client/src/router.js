import Vue from 'vue'
import Router from 'vue-router'
import Map from './views/Map.vue'
import Record from './views/Record.vue'
import Table from './views/Table.vue'
import Tiles from './views/Tiles.vue'

Vue.use(Router)

const router = new Router({
	routes: [{
		path: '/',
		name: 'home',
		component: Table
	}, {
		path: '/map',
		name: 'map',
		component: Map
	}, {
		path: '/record/:id',
		name: 'record',
		component: Record,
		props: true
	}, {
		path: '/table',
		name: 'table',
		component: Table
	}, {
		path: '/tiles',
		name: 'tiles',
		component: Tiles
	}]
});

export default router

import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import '@mdi/font/css/materialdesignicons.min.css'
import VuetifyConfirm from 'vuetify-confirm'
import colors from 'vuetify/es5/util/colors'

Vue.config.productionTip = false

Vue.use(Vuetify, {
	theme: {
        primary: colors.blue.darken1, 
        secondary: colors.blue.lighten4, 
        accent: colors.indigo.base 
    }
});
Vue.use(VuetifyConfirm);

new Vue({
	router,
	store,
	render: h => h(App)
}).$mount('#app')

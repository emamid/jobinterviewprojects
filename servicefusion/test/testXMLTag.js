const {XMLTag } = require('./XMLTag');

// XMLObject x = new XMLObject();
// XMLTag root = x.GetRoot();
// root.SetName("People");
// XMLTag person = root.NewTag("Person");
// person.AddAttribute("id", "2323");
// person.AddAttribute("DOB", "1/1/1901");
// XMLTag fname = person.NewTag("FirstName", "John");
// XMLTag lname = person.NewTag("LastName", "Smith");
// XMLTag address = person.NewTag("address");
// address.NewTag("street", "123 NW 45th street");
// address.NewTag("city", "Gaithersburg");
// address.NewTag("zip", "21234");
// address.NewTag("state", "MD");
// x.Print();
 

x = new XMLTag();

let root = x.getRoot();

root.setName('People');

let person = root.newTag('Person');

person.addAttribute('id', '2323');
person.addAttribute('DOB', '1/1/1901');

let fname = person.newTag('FirstName', 'John');
let lname = person.newTag('LastName', 'Smith');
let address = person.newTag('address');
address.newTag('street', '123 NW 45th street');
address.newTag('city', 'Gaithersburg');
address.newTag('zip', '21234');
address.newTag('state', 'MD');

let newPerson = root.newTag('Person');
newPerson.addAttribute('id', '2324');
newPerson.addAttribute('DOB', '6/23/1968');
newPerson.newTag('FirstName', 'David');
newPerson.newTag('LastName', 'Emami');
newAddress = newPerson.newTag('address');
let street = newAddress.newTag('street', '607 Haltwhistle Court');
street.newTag('apartment', '#123');
street.addAttribute('direction', 'northwest');
newAddress.newTag('city', 'Las Vegas');
newAddress.newTag('zip', '89178');
newAddress.newTag('state', 'NV');
console.log(x.print());
// Write a simple program to store some simple XML - basic tags and attributes. 
// Don't worry about the encoding or XML features except for the tags and tag attributes. 
// You cannot use any existing XML libraries or APIs. Your code should compile and run and demonstrate the abilities of the your XML classes.


// Example code in C#

// XMLObject x = new XMLObject();
// XMLTag root = x.GetRoot();
// root.SetName("People");
// XMLTag person = root.NewTag("Person");
// person.AddAttribute("id", "2323");
// person.AddAttribute("DOB", "1/1/1901");
// XMLTag fname = person.NewTag("FirstName", "John");
// XMLTag lname = person.NewTag("LastName", "Smith");
// XMLTag address = person.NewTag("address");
// address.NewTag("street", "123 NW 45th street");
// address.NewTag("city", "Gaithersburg");
// address.NewTag("zip", "21234");
// address.NewTag("state", "MD");
// x.Print();
 
// Example output
// <people>
//     <person id="2323" DOB="1/1/1901">
//         <firstname>John</firstname>
//         <lastname>Smith</lastname>
//         <address>
//             <street>123 NW 45th street</street>
//             <city>Gaithersuburg</city>
//             <zip>21234</zip>
//             <state>MD</state>
//         </address>
//     </person>
// </people>

class XMLTag {

    constructor(name, value, parent) {
        this.name = name ? name.toLowerCase() : name;
        this.value = value ? value.trim() : value;
        this.parent = parent;
        this.children = [];
        this.attributes = {};
    }

    newTag(name, value) {
        let parent = this;
        let child=new XMLTag(name, value, parent);
        this.children.push(child);
        return child;
    }

    addAttribute(name, value) {
        this.attributes[name] = value;
    }

    getRoot() {
        if (this.parent) {
            return this.parent.getRoot();
        } else {
            return this;
        }

    }

    print(depth = 0) {
        let result = '';
        let indent = ' '.repeat(depth*4);
        let startTag = `<${this.name}`;
        for (let attributeName in this.attributes) {
            startTag = `${startTag} ${attributeName}="${this.attributes[attributeName]}"`;
        }
        startTag += '>';
        let endTag = `</${this.name}>`;
        if (this.children.length) {
//            console.log(indent, startTag);
            result = indent + startTag + '\n';
            if (this.value) {
                result += (indent + this.value + '\n');
            }
            for (let child of this.children) {
//                child.print(depth+1);
                result += child.print(depth+1);
            }
//            console.log(indent,endTag);
            result += (indent + endTag + '\n');
        } else {
//            console.log(indent,startTag,this.value,endTag);
            result = indent + startTag + this.value + endTag + '\n';
        }
        return result;
    }

    setName(name) {
        this.name = name ? name.toLowerCase() : name;
    }
}

module.exports = {
    XMLTag
}
